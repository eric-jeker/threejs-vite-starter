import { Debug } from '../utilities/Debug';

export interface Animatable {
  onUpdate: Function,
}

const AnimationSystem = {
  get entities(): Animatable[] {
    return this._entities;
  },

  get firstTickTime(): number | null {
    return this._firstTickTime;
  },

  set firstTickTime(value: number | null) {
    this._firstTickTime = value;
  },

  get delta(): number {
    return this._delta;
  },

  set delta(value: number) {
    this._delta = value;
  },

  get elapsed(): number {
    return this._elapsed;
  },

  set elapsed(value: number) {
    this._elapsed = value;
  },

  get lastTickTime(): number | null {
    return this._lastTickTime;
  },

  set lastTickTime(value: number | null) {
    this._lastTickTime = value;
  },

  set running(value: boolean) {
    this._running = value;
  },

  get running(): boolean {
    return this._running;
  },

  _firstTickTime: null,
  _lastTickTime: null,
  _elapsed: 0,
  _delta: 16,
  _running: false,
  _entities: [],

  init() {
    // Add debug
    const animation = Debug.gui.addFolder('Animation');
    animation.add(this, 'start');
    animation.add(this, 'stop');

    // refresh when changing values in lil.GUI
    Debug.gui.onFinishChange(() => {
      this.tick();
    });
  },

  /**
   * @description Stop the execution of the update loop.
   */
  stop(): void {
    this.running = false;
  },

  /**
   * @description Start the execution of the update loop.
   */
  start(): void {
    // Always reset in case engine was stopped and restarted
    this.firstTickTime = this.lastTickTime = performance.now();

    // Start ticking!
    this.running = true;

    window.requestAnimationFrame(() => {
      this.tick();
    });
  },

  tick() {
    if (this.running) {
      const now = performance.now();
      this.delta = now - (this.lastTickTime || 0);
      this.lastTickTime = now;
      this.elapsed = this.lastTickTime - (this.firstTickTime || 0);

      // call each animatable to update them
      this.entities.forEach((entity: Animatable) => {
        entity.onUpdate();
      });

      window.requestAnimationFrame(() => {
        this.tick();
      });
    }
  },

  addAnimatable(entity: Animatable) {
    this._entities.push(entity);
  },

  dispose() {
    this._entities = [];
  },
}


export { AnimationSystem };
