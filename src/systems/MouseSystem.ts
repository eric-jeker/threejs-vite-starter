import * as THREE from 'three';

const MouseSystem = {
  _pos: new THREE.Vector2(0, 0),

  get pos(): number  {
    return this._pos;
  },

  get ev(): number  {
    return this._ev;
  },

  init(target) {
    this._target = target;
  },

  addEventListener() {
    this._target.addEventListener('mousemove', (e) => {
      this._ev = e;
      this._pos = new THREE.Vector2(e.offsetX, e.offsetY);
    });
  }
}

export { MouseSystem };
