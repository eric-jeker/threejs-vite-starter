export interface Resizeable {
  onResize: Function;
}

const ScreenSystem = {
  get pixelRatio(): number {
    return this._pixelRatio;
  },

  set height(value: number) {
    this._height = value;
  },

  get height(): number {
    return this._height;
  },

  set width(value: number) {
    this._width = value;
  },

  get width(): number {
    return this._width;
  },

  get entities(): any {
    return this._entities;
  },

  get aspectRatio(): number {
    return this._aspectRatio;
  },

  _width: window.innerWidth,
  _height: window.innerHeight,
  _aspectRatio: window.innerHeight / window.innerWidth,
  _pixelRatio: Math.min(window.devicePixelRatio, 2),
  _entities: [],

  init(width, height) {
    this._width = width;
    this._height = height;
    this._aspectRatio = height / width;
    this._widthRatio = width / window.innerWidth;
    this._heightRatio = height / window.innerHeight;
  },

  addResizeable(entity: Resizeable) {
    this._entities.push(entity);
  },

  /**
   * Resizing strategy, by default we apply the same ratio as initiated
   */
  onResize() {
    this._width = window.innerWidth * this._widthRatio;
    this._height = window.innerHeight * this._heightRatio;
  },

  /**
   * Will set the width and height to the window inner sizes.
   */
  addEventListener() {
    window.addEventListener('resize', () => {
      this.onResize();

      this.entities.forEach((entity: Resizeable) => {
        entity.onResize();
      });
    });
  },

  dispose() {
    this._entities = [];
  },
}

export { ScreenSystem };
