#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159265359
#define TWO_PI 6.28318530718
#define HALF_PI 1.570796326795

uniform vec2 u_resolution;  // Canvas size (width,height)
uniform vec2 u_mouse;       // mouse position in screen pixels
uniform float u_time;       // Time in seconds since load

#include palettes/dracula;
#include shaping/easing;
#include colors/transform;

float plot (float pct, vec2 st){
    return smoothstep(pct - 0.005, pct, st.y) - smoothstep(pct, pct + 0.005, st.y);
}

float square(float width, vec2 st) {
    vec2 bl = step(vec2(width), st);
    vec2 tr = step(vec2(width), 1.0 - st);

    return bl.x * bl.y * tr.x * tr.y;
}


float pattern(vec2 p) {
    
}


void main()
{
    vec2 st = gl_FragCoord.xy / u_resolution.xy;
    vec3 color = vec3(BLACK);

    float pct1 = 1.0 - bounceOut(pow(st.x, 2.0));
    float pct2 = bounceIn(st.x);
    float pct3 = circularIn(st.x);

    // vec2(0.5) is the center
    vec2 to_center = vec2(0.5) - st;
    float angle = atan(to_center.x, to_center.y);
    float radius = length(to_center) * 2.0;

    // Map the angle (-PI to PI) to the Hue (from 0 to 1)
    // and the Saturation to the radius
    color = hsb2rgb(vec3((angle/TWO_PI)+0.5,radius,1.0));

    // Draw borders
    color = mix(color, BLACK, square(0.1, st) * abs(square(0.2, st) - 1.0));

    // vertical white line
    color = mix(color, WHITE, step(0.35, st.x) - step(0.4, st.x));
    color = mix(color, WHITE, step(0.35, st.x) - step(0.4, st.x));
    // square
    color = mix(color, WHITE, square(0.25, vec2(st.x - 0.5, st.y - 0.5)) * abs(square(0.28, vec2(st.x - 0.5, st.y - 0.5)) - 1.0));
    color = mix(color, WHITE, square(0.25, vec2(st.x - 0.5, st.y - 0.5)) * abs(square(0.28, vec2(st.x - 0.5, st.y - 0.5)) - 1.0));

    // Plot transition lines for each channel
    color = mix(color, BLACK, plot(pct1, st));
    color = mix(color, BLACK, plot(pct2, st));
    color = mix(color, BLACK, plot(pct3, st));

    float pct = distance(st, vec2(0.5));
    color = mix(color, BLACK, step(0.2, pct)-step(0.22, pct));

    // final color of the pixel
    gl_FragColor = vec4(color, 1.0);
}
