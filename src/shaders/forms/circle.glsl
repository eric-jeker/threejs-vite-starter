// circle using dot() instead of distance()
float circle(in vec2 st, in vec2 pos, in float radius){
    vec2 dist = st - pos;

	return 1.0 - smoothstep(radius-(radius*0.01), radius+(radius*0.01), dot(dist, dist)*4.0);
}
