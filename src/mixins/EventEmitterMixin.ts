type Constructor = new (...args: any[]) => {};

function EventEmitterMixin<TBase extends Constructor>(Base: TBase) {
  return class extends Base {
    private _handlers: Record<string, Array<Function>> = {};

    /**
     * Subscribe to event, usage:
     *  menu.on('select', function(item) { ... }
     */
    on(eventName: string, handler: Function) {
      if (!(eventName in this._handlers)) {
        this._handlers[eventName] = [];
      }

      this._handlers[eventName].push(handler);
    }

    /**
     * Cancel the subscription, usage:
     *  menu.off('select', handler)
     */
    off(eventName: string, handler: Function) {
      if (!(eventName in this._handlers)) {
        return;
      }

      this._handlers[eventName] = this._handlers.eventName.filter((h) => h !== handler);
    }

    /**
     * Generate an event with the given name and data
     *  this.trigger('select', data1, data2);
     */
    trigger(eventName: string, ...args: []) {
      if (!(eventName in this._handlers)) {
        return;
      }

      // call the handlers
      this._handlers[eventName].forEach(handler => handler.apply(this, args));
    }
  }
}

export { EventEmitterMixin };
