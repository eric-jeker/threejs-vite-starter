import * as dat from 'lil-gui';

const Debug = {
  _enable: true,
  _gui: new dat.GUI({
    width: 340,
  }),

  enable() {
    this._enable = true;
    this._gui.show(true);
  },

  toggle() {
    this._enable = !this._enable;
    this._gui.show(this._enable);
  },

  disable() {
    this._gui.hide();
  },

  get isEnabled() {
    return this._enable;
  },

  get gui() {
    return this._gui;
  },
}

export { Debug };
