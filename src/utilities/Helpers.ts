
export function degreesToRadians (degrees) {
  return degrees * Math.PI / 180;
}

const Helpers = {
  degreesToRadians
};

export default Helpers;

