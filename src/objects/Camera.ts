import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { degreesToRadians } from '../utilities/Helpers.js';
import { ScreenSystem } from '../systems/ScreenSystem';

class Camera {
  private _axesHelper: THREE.AxesHelper;
  private _instance: THREE.PerspectiveCamera;
  private _controls: OrbitControls;

  get axesHelper(): THREE.AxesHelper {
    return this._axesHelper;
  }

  set axesHelper(value: THREE.AxesHelper) {
    this._axesHelper = value;
  }

  get controls(): OrbitControls {
    return this._controls;
  }

  get instance(): THREE.PerspectiveCamera {
    return this._instance;
  }

  constructor(private _fov) {
    this._instance = new THREE.PerspectiveCamera(_fov, ScreenSystem.aspectRatio);

    this._instance.position.x = 0;
    this._instance.position.y = this.getYFromWidth(ScreenSystem.width);
    this._instance.position.z = 0;
    this._instance.lookAt(0, 0, 0);
  }

  addControls(canvas: HTMLCanvasElement) {
    this._controls = new OrbitControls(this.instance, canvas);
    this._controls.enableDamping = true;
  }

  addAxes(size: number = 3) {
    this._axesHelper = new THREE.AxesHelper(size);
    return this._axesHelper;
  }

  getYFromWidth(width) {
    return width / 2 / Math.tan(degreesToRadians(this._fov / 2)) / 2;
  }

  onResize() {
    this._instance.position.y = this.getYFromWidth(ScreenSystem.width);
    this._instance.aspect = ScreenSystem.aspectRatio;
    this._instance.updateProjectionMatrix();
  }

  onUpdate() {
    // reposition the camera according the screen
    this._instance.position.y = this.getYFromWidth(ScreenSystem.width);
    // if exists, update the controls
    this._controls && this.controls.update();
  }

  onDispose() {
    this._controls && this.controls.dispose();
    this._axesHelper && this.axesHelper.dispose();
  }
}

export { Camera };
