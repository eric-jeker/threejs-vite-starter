import * as THREE from 'three';

import { Debug } from '../utilities/Debug';
import { MouseSystem } from '../systems/MouseSystem';
import { Animatable, AnimationSystem } from '../systems/AnimationSystem';
import { ScreenSystem } from '../systems/ScreenSystem';

import vertexShader from '../shaders/plane.vert';
import fragmentShader from '../shaders/ovomind.frag';

export interface Disposable {
  onDispose: Function;
}

class Plane implements Disposable, Animatable {
  get mesh(): THREE.Mesh {
    return this._mesh;
  }

  private _geometry: THREE.PlaneGeometry;
  private _material: THREE.ShaderMaterial;
  private _mesh: THREE.Mesh;

  constructor(private _parameters) {
    this.render(_parameters);
    Debug.gui.add(this._material, 'wireframe');
  }

  private redraw(parameters) {
    this._geometry = this.createGeometry(parameters);
    this._mesh.geometry.dispose();
    this._mesh.geometry = this._geometry;
  }

  private render(parameters) {
    this._geometry = this.createGeometry(parameters);
    this._material = this.createMaterial();
    this._mesh = this.createMesh(this._geometry, this._material);
  }

  private createMesh(geometry, material) {
    const mesh = new THREE.Mesh(geometry, material);
    mesh.rotateX(-(Math.PI / 2));
    return mesh;
  }

  private createMaterial() {
    return new THREE.ShaderMaterial({
      vertexShader,
      fragmentShader,
      uniforms: {
        u_resolution: { value: new THREE.Vector2(ScreenSystem.width, ScreenSystem.height) },
        u_mouse: { value: MouseSystem.pos },
        u_time: { value: 0 },
      },
      //wireframe: true,
    });
  }

  private createGeometry(parameters) {
    return new THREE.PlaneGeometry(
      ScreenSystem.width,
      ScreenSystem.height,
      ScreenSystem.width / parameters.precision,
      ScreenSystem.height / parameters.precision
    );
  }

  onResize() {
    this._mesh.geometry.dispose();
    this._mesh.geometry = this.createGeometry(this._parameters);
    this._material.uniforms.u_resolution.value = new THREE.Vector2(ScreenSystem.width, ScreenSystem.height);
  }

  onUpdate() {
    this._material.uniforms.u_mouse.value = MouseSystem.pos;
    this._material.uniforms.u_time.value = AnimationSystem.elapsed;
  }

  onDispose() {
    this._mesh.geometry.dispose();
    this._material.dispose();
  }
}

export { Plane };
