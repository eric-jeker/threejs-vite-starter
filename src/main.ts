import './assets/style.css';
import * as THREE from 'three';
import { Camera } from './objects/Camera';
import { Plane } from './objects/Plane';
import { Debug } from './utilities/Debug';
import { MouseSystem } from './systems/MouseSystem';
import { AnimationSystem } from './systems/AnimationSystem';
import { ScreenSystem } from './systems/ScreenSystem';

const canvas = document.querySelector('canvas.webgl');

// create the scene
const scene = new THREE.Scene();

/**
 * Scene setup, this is the size of the window
 * @type {{width: number, height: number}}
 */
ScreenSystem.addEventListener();

// Initialize the animation system
AnimationSystem.init();
AnimationSystem.start();

// Listen to mouse event
MouseSystem.addEventListener();

/**
 * ENTER THE SIMULATION
 */

const parameters = {
  resolution: 0.25,
  precision: 10,
};

// the plane geometry has a different resolution compared to
// the window which has a 1:1 resolution
const plane = new Plane(parameters);
scene.add(plane.mesh);
AnimationSystem.addAnimatable(plane);

/**
 * Camera
 */
const camera = new Camera(parameters, 50);
scene.add(camera.instance);
ScreenSystem.addResizeable(camera);
AnimationSystem.addAnimatable(camera);

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({ canvas });

renderer.setSize(ScreenSystem.width, ScreenSystem.height);
renderer.render(scene, camera.instance);

AnimationSystem.addAnimatable({
  onUpdate: () => {
    // Render
    renderer.render(scene, camera.instance);
  }
});

ScreenSystem.addResizeable({
  onResize: () => {
    renderer.setSize(ScreenSystem.width, ScreenSystem.height);
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
  }
});

